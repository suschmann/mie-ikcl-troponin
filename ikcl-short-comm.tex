
\documentclass{IOS-Book-Article}

\usepackage{mathptmx}
\usepackage{soul}\setuldepth{article}
\usepackage[english]{babel}

\usepackage{multirow}

\def\hb{\hbox to 11.5 cm{}}

\begin{document}

    \pagestyle{headings}
    \def\thepage{}

    \begin{frontmatter}              % The preamble begins here.
        %\pretitle{Pretitle}
        \title{Predicting Cardiac Troponin T from I}

        \markboth{}{January 2022\hb}
        %\subtitle{Subtitle}

        \author[A]{\fnms{Sebastian} \snm{Uschmann}%
        %\thanks{Corresponding Author: Book Production Manager, IOS Press, Nieuwe Hemweg 6B, 1013 BG Amsterdam, The Netherlands; E-mail: bookproduction@iospress.nl.}
        },
        \author[A]{\fnms{Sven} \snm{Festag}},
        \author[B]{\fnms{Michael} \snm{Kiehntopf}}
        \author[B]{\fnms{Boris} \snm{Betz}}
        and
        \author[A]{\fnms{Cord} \snm{Spreckelsen}},

        %\runningauthor{S. Uschmann et al.}
        \address[A]{IMSID, Jena University Hospital, Germany}
        \address[B]{IKCL, Jena University Hospital, Germany}

        \begin{abstract}
            The troponins T and I are important indicators of myocardial infarction.
            On a set of parallel measurements we apply standard statistical and machine learning algorithms to predict troponin T.
            On these data we conclude that neural nets predict best and highlight the importance of sex specific models.
        \end{abstract}

        \begin{keyword}
            troponin, machine learning, feature importance, neural net, A.I.
        \end{keyword}
    \end{frontmatter}
    \markboth{January 2022\hb}{January 2022\hb}
    \thispagestyle{empty}
    \pagestyle{empty}
    
    \section{Introduction}
        The coronary markers troponin T (cTnT) and troponin I (cTnI) are measured via assays.
        Although there are investigations of the clinical application of both\cite{r1} and there exist many intra-assay optimisations\cite{r2} we are not aware of any attempts to directly model their connection.
        The direct translation may be very useful though.
        Clinicians often have an intuitive reasoning based on only one of the troponins.
        Additionally it might be useful to have a translation of troponin levels to merge several data sets with distinct sources.
        
        We applied standard machine learning algorithms to check if they outperform standard linear models
        and if a small data set is enough for training.
        Further we check for the importance of sex and other modelling features.

    \section{Methods}
        We obtained an exceptional data set from the Institute of Clinical Chemistry of the Jena University Hospital.
        After merging and validating the data, we got $187$ data samples consisting of the simultaneous measurement\footnote{Methods: ``Elecsys Troponin T hs'' and ``ARCHITECT STAT High Sensitive Troponin-I''} of cTnT and cTnI.
        Additionally we got the blood levels of NT-proBNP, albumin, bilirubin, urea, creatinine, sodium, procalcitonin as well as age, sex and bmi of the unique patients.
        
        We evaluated the performance of four approaches of predicting the level of cTnT based on the other features: plain linear regression, spline regression, random forest regression and feed-forward neural network.
        The performance measure is the MSE of the same 10-fold cross-validation.
%
        %To analyse the influence of the patient's sex on the prediction performance, two additional experiment were conducted.
        We also analysed the influence of the patient's sex on model performance. 
        To this end, a test set comprising only male patients (size: 30) was fixed. Following this, two feed-forward nets were trained, one using only data of other male patients (size: 101) and another additionally making use of samples related to females (size: 157). 
        

    \section{Results}
        The results of the experiments are listed in table~\ref{tab:model-rss}.
        The feed-forward net performs best with respect to a cross validation on the full data set.\\
        The prediction of the cTnT level in male blood samples is deteriorated by enlarging the training set with female samples. \\
        Experiments showed that training feed-forward nets restricted to samples with cTnI levels below a certain threshold, improves their performances in this range.
        \begin{table}[ht]
            \centering
            \begin{tabular}{|p{12em} r|} 
                \hline
                Method & MSE \\[-0.2em]
                \hline
                linear  & 2517.38 \\ [-0.4em]
                spline & 9321.55 \\[-0.4em]
                random forests (60 trees) & 3048.46 \\[-0.4em]
                feed forward net & 2167.22 \\
                \hline
            \end{tabular}
            \hspace{0.5em}
            \begin{tabular}{|p{5.5em} p{9em} r|} 
                \hline
                Method & & MSE \\[-0.2em]
                \hline
                & train male \& test male & 1214.47 \\[-0.4em] 
                \multirow{2}{5.5em}{\\[-1em]feed forward \\ net } & train all \& test male  & 1770.33 \\[-0.4em]
                & cTnI $\le 50$ & 334.33 \\[-0.4em]
                & cTnI $\geq 50$ & 8451.37 \\
                \hline
            \end{tabular}
            \caption{Residual sum of squares of different models. Left: models 10-fold cross-validated; neural net consist of $1$ hidden layer with $60$ units.
            Right: neural net with $1$ layer and $40$ hidden units; lower two mse result from 4-fold cross-validation;
            Hyperparameters are a result of grid search;}
            \label{tab:model-rss}
        \end{table}
        
        To analyse the inter-feature dynamics, the Spearmans correlation between each feature pair was computed. The inverted coefficients were used to create a distance matrix as a basis for a hierarchical clustering of the features.
        The clustering shows a strong connection between urea, creatinine, cTnI and NT-proBNP. %In combination with the fact that NT-proBNP shows a high permutation feature importance, this explains the the low importance of creatinin. 
     
    \section{Discussion}
        Modelling the relation of cTnT and cTnI is possible.
        Considering the present data set and the respective cross-validation MSE of the models neural nets outperform basic models like linear regression. %considering the present data set and it's limitations.
        Models as splines, which behave badly on the borders, perform worst in our experiment.
        %The validity of the models can be improved by restricting to patients with a specific cTnI range.
        Additionally we identified the importance of defining different models for female and male patients.
        Finally we showed that closely related features with unknown functional connection may conceal respective significant influence. 
        %One should also note that the data set is influenced by selection bias and hence models may not generalise.
        
    
    \section{Conclusions}
        Acquiring data sets of parallel measurements of closely related variables is very fruitful.
        Neural nets are a good starting point to model the close relation of cTnI and cTnT.
        Additionally, the results of our work indicate the importance of sex specific models for the task at hand.
        

    \begin{thebibliography}{99}
        % Untersuchung unter Verwendung beider Angaben
        \bibitem{r1}
        Mumma BE, Casey SD, Dang RK, Polen MK, Kaur JC, Rodrigo J, Tancredi DJ, Narverud RA, Amsterdam EA, Tran N. Diagnostic Reclassification by a High-Sensitivity Cardiac Troponin Assay. Annals of Emergency Medicine. 2020;76:566–579.

        % Testen von Verschiedenen Messmethoden desselben Troponins
        \bibitem{r2}
        Tsui AKY, Lyon ME, van Diepen S, Goudreau BL, Thomas D, Higgins T, Raizman JE, Füzéry AK, Rodriguez-Capote K, Estey M, et al. Analytical Concordance of Diverse Point-of-Care and Central Laboratory Troponin I Assays. The Journal of Applied Laboratory Medicine. 2019;3:764–774.
    \end{thebibliography}
\end{document}
